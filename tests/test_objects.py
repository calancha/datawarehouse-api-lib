import unittest

import responses

import datawarehouse

DW_API = 'http://server/api/1'


class TestObjects(unittest.TestCase):
    # pylint: disable=no-member
    """Test requests are performed correctly."""

    def setUp(self):
        self.dw = datawarehouse.Datawarehouse('http://server', 'my-secret-token')
        self.mock_responses()

    @staticmethod
    def mock_responses():
        """Mock all API calls."""
        def generic_list(grouped_by=None, id_key='id'):
            """Get a generic list of json objects."""
            g_list = [{id_key: 1}, {id_key: 2}]
            if grouped_by:
                g_list = {grouped_by: g_list}
            return {'results': g_list}

        get_endpoints = [
            ('/pipeline/123', {'pipeline_id': 123}),
            ('/issue', generic_list()),
            ('/issue/1', {'id': 1}),
            ('/issue/-/regex', generic_list()),
            ('/test', generic_list()),
            ('/test/2', {'id': 2}),
            ('/kcidb/data/checkouts?'
             'name=checkouts&last_retrieved_id=0&limit=1', {'id': 1}),
            ('/kcidb/data/builds?'
             'name=builds&last_retrieved_id=0&limit=1', {'id': 1}),
            ('/kcidb/data/tests?'
             'name=tests&last_retrieved_id=0&limit=1', {'id': 1}),
            ('/kcidb/checkouts', {'results': [{'id': 1, 'misc': {'iid': 1}}]}),
            ('/kcidb/checkouts/1', {'id': 1, 'misc': {'iid': 1}}),
            ('/kcidb/checkouts/1/all',
             {'checkouts': [{'id': 1, 'misc': {'iid': 1}}],
              'builds': [{'id': 1, 'misc': {'iid': 1}}],
              'tests': [{'id': 1, 'misc': {'iid': 1}}]}),
            ('/kcidb/checkouts/1/builds', {'results': [{'id': 1, 'misc': {'iid': 1}}]}),
            ('/kcidb/checkouts/1/issues', generic_list()),
            ('/kcidb/checkouts/1/issues/1', {'id': 1}),
            ('/kcidb/checkouts/1/issues/occurrences', generic_list()),
            ('/kcidb/checkouts/1/issues/occurrences/1', {'id': 1}),
            ('/kcidb/checkouts/1/recipients',
             {'send_to': ['to@mail.com'], 'send_cc': ['cc@mail.com']}),
            ('/kcidb/builds/1', {'id': 1, 'misc': {'iid': 1}}),
            ('/kcidb/builds/1/tests', {'results': [{'id': 1, 'misc': {'iid': 1}}]}),
            ('/kcidb/builds/1/issues', generic_list()),
            ('/kcidb/builds/1/issues/1', {'id': 1}),
            ('/kcidb/builds/1/issues/occurrences', generic_list()),
            ('/kcidb/builds/1/issues/occurrences/1', {'id': 1}),
            ('/kcidb/tests/1', {'id': 1, 'misc': {'iid': 1}}),
            ('/kcidb/tests/1/issues', generic_list()),
            ('/kcidb/tests/1/issues/1', {'id': 1}),
            ('/kcidb/tests/1/issues/occurrences', generic_list()),
            ('/kcidb/tests/1/issues/occurrences/1', {'id': 1}),
            ('/kcidb/tests/1/results', {'results': [{'id': 1, 'misc': {'iid': 1}}]}),
            ('/kcidb/testresults/1', {'id': 1, 'misc': {'iid': 1}}),
            ('/kcidb/testresults/1/issues', generic_list()),
            ('/kcidb/testresults/1/issues/1', {'id': 1}),
            ('/kcidb/testresults/1/issues/occurrences', generic_list()),
            ('/kcidb/testresults/1/issues/occurrences/1', {'id': 1}),
        ]

        for endpoint, payload in get_endpoints:
            responses.add(responses.GET, DW_API + endpoint, json=payload)

        post_endpoints = [
            ('/kcidb/submit', {}),
            ('/kcidb/checkouts/1/issues', {'id': 2}),
            ('/kcidb/builds/1/issues', {'id': 2}),
            ('/kcidb/tests/1/issues', {'id': 2}),
            ('/kcidb/checkouts/1/actions/triaged', {}),
            ('/kcidb/builds/1/actions/triaged', {}),
            ('/kcidb/tests/1/actions/triaged', {}),
            ('/kcidb/testresults/1/actions/triaged', {}),
        ]

        for endpoint, payload in post_endpoints:
            responses.add(responses.POST, DW_API + endpoint, json=payload)

    @responses.activate
    def test_get_pipeline(self):
        """Test get pipeline."""

        pipeline = self.dw.pipeline.get(123)
        self.assertEqual(123, pipeline.pipeline_id)

        self.assertEqual(
            responses.calls[0].request.url,
            DW_API + '/pipeline/123'
        )

    @responses.activate
    def test_get_issue_regex(self):
        """Test issue_regex."""
        self.dw.issue_regex.list()

    @responses.activate
    def test_get(self):
        """Test tests."""
        self.dw.test.list()
        self.dw.test.get(2)

    @responses.activate
    def test_kcidb_data(self):
        """Test kcidb endpoints."""
        pagination = '&last_retrieved_id=0&limit=1'
        self.dw.kcidb.data.get(name='checkouts', last_retrieved_id=0, limit=1)
        self.dw.kcidb.data.get(name='builds', last_retrieved_id=0, limit=1)
        self.dw.kcidb.data.get(name='tests', last_retrieved_id=0, limit=1)

        self.assertListEqual(
            [
                DW_API + '/kcidb/data/checkouts?name=checkouts' + pagination,
                DW_API + '/kcidb/data/builds?name=builds' + pagination,
                DW_API + '/kcidb/data/tests?name=tests' + pagination,
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_issues(self):
        """Test issues."""
        self.dw.issue.list()
        self.dw.issue.list(resolved=False)
        self.dw.issue.get(1)

        self.assertListEqual(
            [
                DW_API + '/issue',
                DW_API + '/issue?resolved=False',
                DW_API + '/issue/1',
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_submit(self):
        """Test kcidb submit."""
        self.dw.kcidb.submit.create(data={'key': 'value'})
        self.assertEqual(
            [
                (DW_API + '/kcidb/submit', '{"data": {"key": "value"}}'),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb(self):
        """Test kcidb."""
        self.dw.kcidb.checkouts.list()
        rev = self.dw.kcidb.checkouts.get(1)
        rev.builds.list()
        build = self.dw.kcidb.builds.get(1)
        build.tests.list()
        test = self.dw.kcidb.tests.get(1)
        test.results.list()
        self.dw.kcidb.testresults.get(1)

        self.assertListEqual(
            [
                DW_API + '/kcidb/checkouts',
                DW_API + '/kcidb/checkouts/1',
                DW_API + '/kcidb/checkouts/1/builds',
                DW_API + '/kcidb/builds/1',
                DW_API + '/kcidb/builds/1/tests',
                DW_API + '/kcidb/tests/1',
                DW_API + '/kcidb/tests/1/results',
                DW_API + '/kcidb/testresults/1',
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_issues(self):
        """Test kcidb issues."""
        rev = self.dw.kcidb.checkouts.get(1)
        build = self.dw.kcidb.builds.get(1)
        test = self.dw.kcidb.tests.get(1)
        testresult = self.dw.kcidb.testresults.get(1)

        responses.calls.reset()
        rev.issues.list()
        rev.issues.get(1)
        build.issues.list()
        build.issues.get(1)
        test.issues.list()
        test.issues.get(1)
        testresult.issues.list()
        testresult.issues.get(1)

        self.assertListEqual(
            [
                DW_API + '/kcidb/checkouts/1/issues',
                DW_API + '/kcidb/checkouts/1/issues/1',
                DW_API + '/kcidb/builds/1/issues',
                DW_API + '/kcidb/builds/1/issues/1',
                DW_API + '/kcidb/tests/1/issues',
                DW_API + '/kcidb/tests/1/issues/1',
                DW_API + '/kcidb/testresults/1/issues',
                DW_API + '/kcidb/testresults/1/issues/1',
            ],
            [call.request.url for call in responses.calls]
        )

        responses.calls.reset()
        rev.issues.create(issue_id=2)
        build.issues.create(issue_id=2)
        test.issues.create(issue_id=2)

        self.assertEqual(
            [
                (DW_API + '/kcidb/checkouts/1/issues', '{"issue_id": 2}'),
                (DW_API + '/kcidb/builds/1/issues', '{"issue_id": 2}'),
                (DW_API + '/kcidb/tests/1/issues', '{"issue_id": 2}'),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_checkout_action_triaged(self):
        """Test kcidb action triaged."""
        checkout = self.dw.kcidb.checkouts.get(1)
        checkout.action_triaged.create()
        self.assertEqual(
            [
                (DW_API + '/kcidb/checkouts/1', None),
                (DW_API + '/kcidb/checkouts/1/actions/triaged', None),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_build_action_triaged(self):
        """Test kcidb action triaged."""
        build = self.dw.kcidb.builds.get(1)
        build.action_triaged.create()
        self.assertEqual(
            [
                (DW_API + '/kcidb/builds/1', None),
                (DW_API + '/kcidb/builds/1/actions/triaged', None),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_test_action_triaged(self):
        """Test kcidb action triaged."""
        test = self.dw.kcidb.tests.get(1)
        test.action_triaged.create()
        self.assertEqual(
            [
                (DW_API + '/kcidb/tests/1', None),
                (DW_API + '/kcidb/tests/1/actions/triaged', None),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_testiresult_action_triaged(self):
        """Test kcidb action triaged."""
        testresult = self.dw.kcidb.testresults.get(1)
        testresult.action_triaged.create()
        self.assertEqual(
            [
                (DW_API + '/kcidb/testresults/1', None),
                (DW_API + '/kcidb/testresults/1/actions/triaged', None),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_checkout_recipients(self):
        """Test checkout recipients."""
        recipients = self.dw.kcidb.checkouts.get(1).recipients.get()
        self.assertEqual(['to@mail.com'], recipients.send_to)
        self.assertEqual(['cc@mail.com'], recipients.send_cc)

    @responses.activate
    def test_checkout_all(self):
        """Test checkout all objects."""
        kcidb_file = self.dw.kcidb.checkouts.get(1).all.get()

        self.assertEqual(
            [
                (DW_API + '/kcidb/checkouts/1', None),
                (DW_API + '/kcidb/checkouts/1/all', None),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

        self.assertIsInstance(kcidb_file.checkouts[0], datawarehouse.objects.KCIDBCheckout)
        self.assertIsInstance(kcidb_file.builds[0], datawarehouse.objects.KCIDBBuild)
        self.assertIsInstance(kcidb_file.tests[0], datawarehouse.objects.KCIDBTest)

        # Make sure managers still work on these objects
        responses.calls.reset()
        kcidb_file.checkouts[0].issues.list()
        kcidb_file.builds[0].issues.list()
        kcidb_file.tests[0].issues.list()

        self.assertListEqual(
            [
                DW_API + '/kcidb/checkouts/1/issues',
                DW_API + '/kcidb/builds/1/issues',
                DW_API + '/kcidb/tests/1/issues',
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_issues_occurrences(self):
        """Test kcidb issues occurrences."""
        rev = self.dw.kcidb.checkouts.get(1)
        build = self.dw.kcidb.builds.get(1)
        test = self.dw.kcidb.tests.get(1)
        testresult = self.dw.kcidb.testresults.get(1)

        responses.calls.reset()
        rev.issues_occurrences.list()
        rev.issues_occurrences.get(1)
        build.issues_occurrences.list()
        build.issues_occurrences.get(1)
        test.issues_occurrences.list()
        test.issues_occurrences.get(1)
        testresult.issues_occurrences.list()
        testresult.issues_occurrences.get(1)

        self.assertListEqual(
            [
                DW_API + '/kcidb/checkouts/1/issues/occurrences',
                DW_API + '/kcidb/checkouts/1/issues/occurrences/1',
                DW_API + '/kcidb/builds/1/issues/occurrences',
                DW_API + '/kcidb/builds/1/issues/occurrences/1',
                DW_API + '/kcidb/tests/1/issues/occurrences',
                DW_API + '/kcidb/tests/1/issues/occurrences/1',
                DW_API + '/kcidb/testresults/1/issues/occurrences',
                DW_API + '/kcidb/testresults/1/issues/occurrences/1',
            ],
            [call.request.url for call in responses.calls]
        )
